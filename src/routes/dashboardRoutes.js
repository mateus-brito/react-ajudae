import Index from 'pages/index.js';
import About from 'pages/about.js';
import HowToHelp from 'pages/howToHelp.js';
import Contact from 'pages/contact.js';

export const internRoutes = [
    { exact: true, path: "/", component: Index },
    { exact: true, path: "/como-ajudar", component: HowToHelp },
    { exact: true, path: "/sobre", component: About },
    { exact: true, path: "/contato", component: Contact },
];