import React, {useEffect, useState, useRef} from 'react';
import Header from "../components/header";
import { Switch, Route } from "react-router-dom";
import { internRoutes } from "routes/dashboardRoutes.js";
import { userService } from '_services';
import { connect } from 'react-redux';
import withWidth from '@material-ui/core/withWidth';
import Auth from 'components/auth';
import Modal from 'react-modal';
import { userActions } from '_actions';
import ViewerProfile from "components/ViewerProfile"

import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import EmailIcon from '@material-ui/icons/Email';

const renderMergedProps = (component, ...rest) => {
    const finalProps = Object.assign({}, ...rest); 
    return (
      React.createElement(component, finalProps)
    );
  }

const PropsRoute = ({ component, ...rest }) => {
  return (
    <Route {...rest} render={routeProps => {
      return renderMergedProps(component, routeProps, rest);
    }}/>
  );
}

var selected_callback = () => {};

function DefaultLayout(props) {
    
    const markersRef = useRef([]);
    const [user, setUser ] = useState({});
    
    const [markers, setMarkers] = useState([]);
    
    const [target_profile, setTargetProfile] = useState(-1);

    const [isOpenRegister, setAddMarkerIsOpen] = useState(false);
    const [isOpenProfile, setIsOpenProfile] = useState(false);

  useEffect(() => {
    if (props.call_after_login instanceof Function)
    {
      selected_callback = props.call_after_login;
    }
    
  }, [props.call_after_login])

  useEffect(() => {
    if( props.loggedIn === true)
    {
      selected_callback();
    }
  }, [props.loggedIn])

  useEffect(() => {
    if(props.isOpenDialog === false )
    {
      selected_callback = () => {};
    }
  },[props.isOpenDialog]);

  useEffect(() => {
    userService.sendRequest('/api/marker/get', 'GET').then((res) => {
      setMarkers( res );
    });
  }, [props.refreshMarkers])

    useEffect(() => {
        const user_temp = JSON.parse(localStorage.getItem('user'));
        setUser( user_temp )
    },[props.loggingIn]);

    const openRegisterMarker = () => {
        setAddMarkerIsOpen(true);
    }

    const closeAuthDialog = () => {
        const { dispatch } = props;
        dispatch(userActions.closeDialog());
    }

    const showProfile = ( user_id ) => {
      setIsOpenProfile(true);
      setTargetProfile(user_id);
    }

    const { isOpenDialog, } = props;

    return (
    <div style={styles.container}>
      <Header
      location={props.location}
      markers={markers}
      markersRef={markersRef}
      showProfile={showProfile}
      openAddMarker={ openRegisterMarker } />

        <Switch>
        {internRoutes.map((prop, key) => {
            if(prop.exact){
                return(
                <PropsRoute 
                exact
                setOpenRegister={setAddMarkerIsOpen}
                showProfile={showProfile}
                target_profile={target_profile}
                isOpenRegister={isOpenRegister}
                markersRef={markersRef}
                markers={markers}
                path={prop.path} 
                component={prop.component} 
                key={key} />);
            }
            else{
                return <PropsRoute path={prop.path} component={prop.component} key={key} />;
            }
            
        })}
        </Switch>

        <div style={{
          padding: 8,
          width:'calc(100% - 16px)',
          textAlign: 'right',
          borderTop: '1px solid rgb(227, 227, 227)',
          backgroundColor: '#383838'
          }}>

            
          <div style={{
            lineHeight: '12px',
            alignContent: 'center',
            alignItems: 'center',
            display: 'flex',
            justifyContent: 'end',
          }}>
          <span style={{fontSize: '12px', color: '#fff', marginRight: '10px'}}>Acesse nossas redes sociais</span>
            <FacebookIcon style={{color: "rgb(196, 196, 196)"}} />
            <TwitterIcon style={{color: "rgb(196, 196, 196)"}} />
            <InstagramIcon style={{color: "rgb(196, 196, 196)"}} />
            <EmailIcon style={{color: "rgb(196, 196, 196)"}} />
          </div>

        </div>

        <Modal
            isOpen={(isOpenDialog && user==null)}
            onRequestClose={() => {closeAuthDialog()}}
            style={styles.modal}
            contentLabel="Auth"
            >
              <Auth />
        </Modal>

        <Modal
          isOpen={isOpenProfile}
          onRequestClose={() => {setIsOpenProfile(false)}}
          contentLabel="Profile"
          >
              <ViewerProfile user_id={target_profile} close={() => setIsOpenProfile(false)} />
          </Modal>

    </div>
  );
}

const styles = {
  container:{
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh'
  },
  modalContainer: {
    display: 'flex',
    width: '100%',
    height: '100%',
    flexDirection: 'row'
  }
}

function mapStateToProps(state) {
  const { isOpenDialog, loggingIn, loggedIn, call_after_login } = state.authentication;
  const { refreshMarkers } = state.alert;

  return {
    refreshMarkers,
    call_after_login,
    loggedIn,
    loggingIn,
    isOpenDialog,
  };
}

const connectedDefaultLayout = connect(mapStateToProps)(DefaultLayout);

export default withWidth()(connectedDefaultLayout);
