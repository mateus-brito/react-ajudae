import $ from 'jquery'; 

export const userService = {
    login,
    register,
    logout,
    sendFormData,
    sendRequest
};

function login(email, password) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
        body: JSON.stringify({ email, password })
    };
    return fetch(`${process.env.REACT_APP_API_URL}/login`, requestOptions)
        .then(handleResponse)
        .then(user => {
            // login successful if there's a jwt token in the response
            if (user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
            }
            
            return user;
        });
}

function sendFormData(url, option, formData){
    let user = JSON.parse( localStorage.getItem("user") );
    const token = user != null ? user.token : "";

    const requestOptions = {
        method: option,
        headers: {
            'Authorization': 'Bearer ' + token
        },
        body: formData
    };

    return fetch(`${process.env.REACT_APP_API_URL}${url}`, requestOptions)
        .then(response => {
            return response;
    });
}

function sendRequest (url, option, params = {}) {
    let user = JSON.parse( localStorage.getItem("user") );
    const token = user != null ? user.token : "";
    let GET_PARAMS = "";

    const requestOptions = {
        method: option,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify( params )
    };
    if(option.toUpperCase() === 'GET')
    {
        GET_PARAMS = "?"+$.param( params );
        delete requestOptions['body'];
    }
    return fetch(`${process.env.REACT_APP_API_URL}${url}`+GET_PARAMS, requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
    });
}

function register(email, username, password) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify({ email, username, password })
    };
    return fetch(`${process.env.REACT_APP_API_URL}/register`, requestOptions)
        .then(handleResponse)
        .then(user => {
            // login successful if there's a jwt token in the response
            if (user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
            }
            
            return user;
    });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function handleResponse(response) {
    return response.json().then(data => {
        if (!response.ok) {
            if (response.status === 401 && localStorage.getItem('user') != null) {
                // auto logout if 401 response returned from api
                logout();
                window.location.reload(true);
            }
            const error = (data && data.error) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}