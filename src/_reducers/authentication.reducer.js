import { userConstants } from '../_constants';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        isOpenDialog: true,
        user: action.user
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        isOpenDialog: false,
        user: action.user
      };
    case userConstants.LOGIN_FAILURE:
      return {
        isOpenDialog: true,
        errorLogin: action.error
      };
    case userConstants.REGISTER_FAILURE:
      return {
        isOpenDialog: true,
        errorRegister: action.error
      };
    case userConstants.LOGOUT:
      return {
        loggingIn: false,
      };
    case userConstants.OPEN_DIALOG:
      return {
        isOpenDialog: true
      };
    case userConstants.CLOSE_DIALOG:
      return {
        isOpenDialog: false
      };
    case userConstants.AFTER_LOGIN_SUCCESS:
      return {
        call_after_login: action.callback
      };
    default:
      return state
  }
}