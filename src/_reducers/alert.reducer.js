import { alertConstants } from '../_constants';

const initialState = { refreshMarkers: false};

export function alert(state = initialState, action) {
  switch (action.type) {
    case alertConstants.UPDATE_MARKERS: 
        return {
            refreshMarkers: true
        };
    
    default:
        return state
    }
}
