import { userConstants } from '../_constants';
import { userService } from '../_services';

export const userActions = {
    setAfterLogin,
    openDialog,
    closeDialog,
    login,
    register,
    logout
};

function openDialog() {
    return dispatch => {
        dispatch(request());
    }
    function request() { return { type: userConstants.OPEN_DIALOG } }
}

function closeDialog() {
    return dispatch => {
        dispatch(request());
    }
    function request() { return { type: userConstants.CLOSE_DIALOG } }
}

function setAfterLogin( callback ){
    return dispatch => {
        dispatch(request(callback));
    }
    function request( func_target ) { return { type: userConstants.AFTER_LOGIN_SUCCESS, callback: func_target } }
}

function login(username, password) {
    return dispatch => {
        dispatch(request({ username }));

        userService.login(username, password)
            .then(
                user => { 
                    dispatch(success(user));
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function register(email, username, password) {
    return dispatch => {
        dispatch(request({ email }));

        userService.register(email, username, password)
            .then(
                user => { 
                    dispatch(success(user));
                },
                error => {
                    dispatch(failure(error));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}