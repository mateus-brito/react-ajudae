import React from 'react';
import RoomIcon from '@material-ui/icons/Room';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';

function HistoricMenu(props) {

    const { markers } = props;

    const getMonth = ( mounth_int ) => {
        var result = "";
        switch(mounth_int){
            case 0:
                result = 'jan';
                break;
            case 1:
                result = 'fev';
                break;
            case 2:
                result = 'mar';
                break;
            case 3:
                result = 'abr';
                break;
            case 4:
                result = 'mai';
                break;
            case 5:
                result = 'jun';
                break;
            case 6:
                result = 'jul';
                break;
            case 7:
                result = 'ago';
                break;
            case 8:
                result = 'set';
                break;
            case 9:
                result = 'out';
                break;
            case 10:
                result = 'nov';
                break;
            case 11:
                result = 'dez';
                break;
            default:
                result = '';
        }
        return result;
    }

    return (
        <div style={styles.menu}>
            <div style={{flexDirection: 'row', display: 'flex'}}>
                <h2 style={styles.title}>Novos locais</h2>
                <span 
                onClick={() => props.changeVisiblity(false)}
                style={{
                    cursor: 'pointer',
                    flex: 1, 
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'flex-end'
                }}>
                    <CloseIcon style={{color: 'rgb(77, 77, 77)'}} />
                </span>
            </div>
            {markers.map((item, index) => {
                const date = new Date(item.updated_on);
                return (
                    <div style={styles.item} key={index} onClick={() => {
                        //const element = props.markersRef.current[index].leafletElement;
                        //element['_events']['click'][0]['fn'](element);
                        //props.markersRef.current[index].leafletElement['_events']['click'][1]();
                        props.markersRef.current[index].props['onClick']();
                        props.markersRef.current[index].leafletElement.openPopup();
                        //props.markersRef.current[index].leafletElement['options']['onClick']();
                    }}>
                        <RoomIcon style={styles.iconCard} />
                        <div style={{flex: 1}}>
                            <h3 style={styles.card_title}>{item['name']}</h3>
                            <span style={styles.card_date}>{`${date.getDate()} ${getMonth(date.getMonth())}, ${date.getFullYear()}`}</span>
                        </div>
                        <div style={{alignSelf: 'baseline',}}>
                        </div>
                    </div>
                )
            })}
        </div>
    )
};

const styles = {
    menu: {
        width: 300,
        zIndex: 1,
        maxHeight: 'calc(100vh - 50px)',
        overflow: 'auto',
        borderRight: '1px solid #bfbfbf'
    },
    item:{
        display: 'flex',
        padding: '5px 5px 10px 5px',
        borderBottom: '1px solid #e6e0e0',
        flexDirection: 'row',
        alignItems: 'center',
        cursor: 'pointer'
    },
    card_title:{
        fontWeight: 'bold',
        color: '#424141',
        fontSize: 12,
        margin: 0,
    },
    card_desc: {
        margin: 0,
        fontSize: 12,
    },
    iconCard:{
        color: "#e83600",
        height: '100%',
        padding: "0 5px"
    },
    card_date:{
        fontWeight: 'bold',
        fontSize: 11,
        color: '#776c6c',
        height: '100%',
        display: 'flex',
        
    },
    title:{
        color: 'rgb(79, 74, 73)',
        fontSize: 14,
        marginLeft: 13
    }
}

HistoricMenu.prototype = {
    changeVisiblity: PropTypes.func,
    markers: PropTypes.Array
}

export default HistoricMenu;