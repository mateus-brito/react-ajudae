import React, { useEffect, useState } from 'react';
import { Map, TileLayer, Marker, MapControl, Popup, withLeaflet } from 'react-leaflet';import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import 'leaflet-geosearch/dist/geosearch.css';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { userService } from '_services';

class SearchMap extends MapControl {

    createLeafletElement() {
      return GeoSearchControl({
        provider: new OpenStreetMapProvider(),
        style: 'bar',
        showMarker: true,
        showPopup: false,
        autoClose: true,
        retainZoomLevel: false,
        animateZoom: true,
        keepResult: false,
        searchLabel: 'search'
      });
    }
}

function ClickInMarker(props){
    const [position, setPosition] = useState([-23.533773, -46.625290]);

    const SearchBar = withLeaflet(SearchMap);

    useEffect(() => {
        
    }, []);

    const clickMarker = (e) => {
        const latlng = e.latlng;
        setPosition([latlng.lat, latlng.lng]);
    }

    const save = () => {
        const { handleChange } = props;
        userService.sendRequest(`/address/geocode`, 'GET', {
            lat: position[0],
            lng: position[1]
        }).then((res) => {
            const updated = {};

            if('country' in res){
                //handleChange('country', res['country']);
                if(res['country'] !== "BR"){
                    return;
                }
            }
            if('rua' in res){
                updated['address'] = res['rua'];
            }
            if('rua_num' in res){
                updated['num_address'] = res['rua_num'];
            }
            if('city' in res){
                updated['city'] = res['city'];
            }
            if('state' in res){
                updated['state'] = res['state'];
            }

            handleChange(updated);
            
        });
        props.close();
    }

    return(
        <div style={{width: '100%', height: '100%'}}>
            <Map
            onClick={clickMarker}
            center={position} zoom={13} style={{width: '100%', height: '100%'}}>
                <SearchBar />
                <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={position}>
                <Popup>
                    A pretty CSS3 popup. <br /> Easily customizable.
                </Popup>
                </Marker>
            </Map>
            <div style={{position: 'absolute', bottom: 25, right: 10, zIndex: 1000}}>
                <Button onClick={props.close} variant="contained">Cancelar</Button>
                <Button onClick={save} color="primary" variant="contained">Salvar</Button>
            </div>
        </div>
    )
}

ClickInMarker.propTypes = {
    handleChange: PropTypes.func,
    close: PropTypes.func,
};

export default ClickInMarker;