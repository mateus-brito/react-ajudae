import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ReactStars from "react-rating-stars-component";
import Previews from './previews.js'
import { userService } from '_services';
import PropTypes from 'prop-types';

function CommentRate(props){

    const [uploadImage, setUploadImage] = useState({});
    const [listComments, setListComments] = useState([]);

    const [rating, setRating] = useState(-1);
    const [errorMessage, setErrorMessage] = useState('');
    const [comment, setComment] = useState('');

    useEffect(() => {
        loadComments();
    }, [])

    const loadComments = () => {
        userService.sendRequest("/api/marker/comment/get", "GET", {
            "user_token": props.token
        }).then((res) => {
            setListComments(res);
        });
    }
    
    const ratingChanged = (newRating) => {
        setRating(newRating);
    };

    const handleChange = (event) => {
        setComment(event.target.value);
    };

    const publicPost = (e) => {
        e.preventDefault();
        if(rating === -1){
            setErrorMessage('É necessário fazer uma avaliação.');
        } else if(comment.length === 0 )
        {
            setErrorMessage('Por favor, escreva um comentário.')
        }else{
            setErrorMessage('');

            const formData = new FormData(); 
            formData.append("token", props.token);
            formData.append("file", uploadImage);
            formData.append("comment", comment);
            formData.append("rating", rating);
            
            userService.sendFormData('/api/marker/comment/public', 'POST', formData).then(() => {
                loadComments();
            })

        }
    }

    return(
        <form onSubmit={publicPost}>
            {listComments.length === 0 ?
            <div style={{padding: 10, display: 'flex', justifyContent: 'center'}}>
                <p style={styles.block}>Seja o primeiro a comentar e compartilhar a sua experiência.</p>
            </div>
            :
            <div style={{padding: 10, marginTop: 5}}>
                Avaliações recentes:
            </div>
            }
            <div style={{margin: 10}}>
                {listComments.map((item) => {
                    return(
                        <div style={{marginBottom: 20, border: '1px solid #000', position: 'relative', padding: 10}}>
                            <div style={{
                                backgroundColor: '#fff',
                                display: 'inline-block',
                                position: 'absolute',
                                top: -19,
                                right: 19,
                                }}>
                                <ReactStars
                                    edit={false}
                                    value={item.rating}
                                    count={5}
                                    size={30}
                                    activeColor="#ffd700"
                                />
                            </div>
                            <div style={{display: 'flex', flexDirection: 'row'}}>
                                <p style={{display: 'flex', flex: 1}}>{item.description}</p>
                                <img alt="foto anexada" style={{marginTop: 20, width: 150, marginBottom: 10}} src={'http://localhost:5000/static/'+item.foto_url} />
                            </div>
                        </div>
                    )
                })}
            </div>
            <Previews setImage={setUploadImage} />
            <div style={{marginLeft: 10, marginBottom: 5}}>
                <ReactStars
                    count={5}
                    onChange={ratingChanged}
                    size={30}
                    activeColor="#ffd700"
                />
            </div>
            <div style={{padding: 5, textAlign: 'right'}}>
                <TextField
                    id="standard-basic"
                    label="Comentário"
                    fullWidth
                    multiline
                    rows={2}
                    variant="filled"
                    value={comment}
                    onChange={handleChange}
                    />
                <Button
                type="submit"
                color="primary"
                style={{marginTop: 10, paddingLeft: 25, paddingRight: 25}}
                variant="contained">Publicar</Button>
            </div>

            {errorMessage.length !== 0 &&
            <div style={{display: 'flex'}}>
                <p style={styles.error}>{errorMessage}</p>
            </div>
            }
            
        </form>
    )
}

const styles = {
    block: {
        padding: 10,
        display: 'flex',
        justifyContent: 'center',
        backgroundColor: 'rgb(158, 151, 234)',
        color: 'rgb(253, 253, 253)'
    },
    error: {
        color: '#fff',
        padding: 10,
        backgroundColor: '#d95d5d',
        marginLeft: 10
    }
}

CommentRate.propTypes = {
    token: PropTypes.string,
}

export default CommentRate;