import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { userService } from '../_services';
import icon_image from 'assets/img/icon.png';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import Autocomplete from '@material-ui/lab/Autocomplete';
import municipios from "address/municipios";
import estados from "address/estados";
import Grid from '@material-ui/core/Grid';
import ClickInMarker from './clickInMap';
import Modal from 'react-modal';

function RegisterMarker(props)
{
    const [addressByClick, setAddressByClick] = useState(false);
    const [formMarker, setFormMarker] = useState({
        name: '',
        address: '',
        num_address: '',
        city: '',
        state: '',
        country: 'Brasil',
        website: '',
        phone: '',
    });

    const handleChange = prop => event => {
        setFormMarker({...formMarker, [prop]: event.target.value});
    };

    const customHandleChange = (updated) => {
        setFormMarker({...formMarker, ...updated});
    };

    const submitForm = (e) => {
        e.preventDefault();

        const params = {
            name: formMarker['name'],
            address: formMarker['address'],
            num_address: formMarker['num_address'],
            city: formMarker['city'],
            state: formMarker['state'],
            country: 'Brasil',
            website: formMarker['website'],
            phone: formMarker['phone']
        };

        userService.sendRequest('/api/marker/create', 'POST', params).then((res) => {
            console.log( res );
            props.close();
        });
    }

    const handleChangeCity = ( value ) => {
        let target_city = '';
        let target_state = "";
    
        if( value != null 
          && typeof value !== "undefined" 
          && typeof value.nome !== "undefined") 
          target_city=value.nome;
    
        if( target_city.length > 0 ){
          const obj_city = municipios.filter((item) => item.nome === target_city );
          const city_state = estados.filter((item) => item.codigo_uf === obj_city[0].codigo_uf)
          target_state = city_state[0].uf;
        } else{
          target_state = formMarker['state'];
        }

        setFormMarker({...formMarker, 'city': target_city, 'state': target_state});
    }
    
    const handleChangeState = (value) => {

        let target_state = '';

        if( value != null 
            && typeof value !== "undefined" 
            && typeof value.uf !== "undefined") 
            target_state=value.uf;

        setFormMarker({...formMarker, 'state': target_state, 'city': ''});
    };

    
    return(
        <div style={{display: 'flex', flexDirection: 'row', height: '100%'}}>            
            <div style={{width: '100%'}}>

                <div style={{marginBottom: 20, backgroundColor: 'rgb(174, 77, 163)', display:'flex', flexDirection: 'row', borderBottom: '1px solid #d9d2d2'}}>
                    <img
                    alt="register icon"
                    style={{
                        height: 45,
                        padding: 10,
                    }} src={icon_image} />
                    <p style={{
                        fontWeight: 'bold',
                        margin: 0,
                        fontSize: 18,
                        padding: '22px 22px 22px 5px', 
                        flex: 1, 
                        color: '#fff'}}>Cadastrar instituição</p>
                    <CloseIcon 
                    onClick={props.close}
                    style={{color: '#fff',padding: 22, cursor: 'pointer'}} />
                </div>
                
                <form style={{padding: 10}} onSubmit={submitForm}>

                    <Grid container spacing={2}>
                        <Grid item md={12}>
                            <TextField 
                            InputLabelProps={{
                                shrink: true,
                            }} 
                            variant="outlined" 
                            fullWidth label="Nome" 
                            onChange={handleChange('name')}
                            value={formMarker['name']}
                            required />
                        </Grid>
                        <Grid item md={6}>
                            <TextField fullWidth label="Rua" onChange={handleChange('address')} value={formMarker['address']} required
                            InputLabelProps={{
                                shrink: true,
                            }} 
                            variant="outlined"  />
                        </Grid>
                        <Grid item md={6}>
                            <TextField fullWidth label="Número" onChange={handleChange('num_address')} value={formMarker['num_address']}
                            InputLabelProps={{
                                shrink: true,
                            }} 
                            variant="outlined"  />
                        </Grid>
                        <Grid item md={4}>
                            <Autocomplete
                            fullWidth
                            onChange={(evt,value) => {handleChangeCity(value)}}
                            value={formMarker['city']}
                            options={municipios}
                            getOptionLabel={(option) => typeof option === 'string' ?  option : option.nome}
                            renderInput={(params) => <TextField {...params} required label="Cidade" InputLabelProps={{ shrink: true }}  variant="outlined" />}
                            />
                        </Grid>
                        <Grid item md={4}>
                            <Autocomplete
                            fullWidth
                            onChange={(evt,value) => {handleChangeState(value)}}
                            value={formMarker['state']}
                            options={estados}
                            getOptionLabel={(option) => typeof option === 'string' ?  option : option.uf}
                            renderInput={(params) => <TextField {...params} required label="Estados" InputLabelProps={{ shrink: true }}  variant="outlined" />}
                            />
                        </Grid>
                        <Grid item md={4}>
                            <TextField fullWidth label="País" disabled onChange={handleChange('country')} value={formMarker['country']} required
                            InputLabelProps={{
                                shrink: true,
                            }} 
                            variant="outlined"  />
                        </Grid>
                        <Grid item md={8}>
                            <TextField fullWidth label="Website" onChange={handleChange('website')} value={formMarker['website']}
                            InputLabelProps={{
                                shrink: true,
                            }} 
                            variant="outlined"  />
                        </Grid>
                        <Grid item md={4}>
                            <TextField fullWidth label="Número para contato" onChange={handleChange('phone')} value={formMarker['phone']}
                            InputLabelProps={{
                                shrink: true,
                            }} 
                            variant="outlined"  />
                        </Grid>
                    </Grid>

                    <div style={{textAlign: 'right', marginTop: 20}}>
                        <Button onClick={() => setAddressByClick(true)}>Não sabe o endereço?</Button>

                        <Button variant="contained" 
                        type="submit"
                        style={{
                            textTransform: 'initial',
                            backgroundColor: 'rgb(225, 72, 165)',
                            color: '#fff'
                        }} >
                        Registrar
                        </Button>
                    </div>
                </form>
            </div>
            <Modal
            isOpen={addressByClick}
            onRequestClose={() => {setAddressByClick(false)}}
            style={styles.modal}
            contentLabel="Example Modal"
            >
                <ClickInMarker
                handleChange={customHandleChange}
                close={() => setAddressByClick(false)}
                 />
            </Modal>
        </div>
    )
}

const styles = {

}

RegisterMarker.propTypes = {
    selectedMarker: PropTypes.object,
    close: PropTypes.func,
};

export default RegisterMarker;