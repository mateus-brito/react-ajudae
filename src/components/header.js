import React, { useLayoutEffect, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import { connect } from 'react-redux';

import Drawer from '@material-ui/core/Drawer';
import MenuIcon from '@material-ui/icons/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import logo_image from "../assets/img/logo.png";
import 'react-dropdown/style.css';
import { userActions } from '_actions';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import { makeStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Link } from "react-router-dom";
import { history } from '_helpers';
import { userService } from '_services';

const useStyles = makeStyles((theme) => ({
    underline: {
        "&&&:before": {
          borderBottom: "none"
        },
        "&&:after": {
          borderBottom: "none"
        }
    }
  }));

function Header(props){

    const [anchorEl, setAnchorEl] = React.useState(null);
    const [user, setUser] = React.useState(null);
    
    const [ isModalOpen, setModalOpen ] = useState(false);
    const classes = useStyles();

    function useWindowSize() {
        const [size, setSize] = useState([0, 0]);
        useLayoutEffect(() => {
          function updateSize() {
            setSize([window.innerWidth, window.innerHeight]);
          }
          window.addEventListener('resize', updateSize);
          updateSize();
          return () => window.removeEventListener('resize', updateSize);
        }, []);
        return size;
    }

    useEffect(() => {
        const user_temp = JSON.parse(localStorage.getItem('user'));
        setUser( user_temp );
    },[props.loggingIn]);

    useEffect(() => {
        const search = props.location.search;
        const params = new URLSearchParams(search);
        const post = params.get('post');
        if( post!=null){
            userService.sendRequest('/api/marker/get', 'GET', {'token': post}).then((res) => {
                if( Array.isArray(res) && res.length > 0 )
                {
                    const index = getIndexFromID( res[0].id );
                    if( props.markersRef.current && props.markersRef.current.length >= index+1 )
                    {
                        props.markersRef.current[index].props['onClick']();
                        props.markersRef.current[index].leafletElement.openPopup();
                        history.push('');
                    }
                }
            });
        }
    },[ (props.markers)])

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const logout = () => {
        const { dispatch } = props;

        dispatch(userActions.logout());
        handleClose();
    }

    const getIndexFromID = ( id ) => {
        const { markers } = props;
        for(let i = 0; i < markers.length; i++)
        {
            if(markers[i].id === id){
                return i;
            }
        }
        return 0;
    }

    const openAuthDialog = () => {
        const { dispatch } = props;
        dispatch(userActions.openDialog());
    }

    const openRegisterDialog = async() => {
        if(user!=null){
            props.openAddMarker();
        }
        else{
            const { dispatch } = props;
            await dispatch( userActions.setAfterLogin(props.openAddMarker) );
            dispatch(userActions.openDialog());
        }
    }

    const showProfile = () => {
        const user_id = user ? user.id : -1;
        props.showProfile( user_id );
    }

    return(
        <>
        {useWindowSize()[0] > 960 ?
        <div style={styles.header}>
            <div style={{...styles.menu, ...{
                flex: 1
            }}}>
                <img alt="logo" src={logo_image} style={styles.logo}/>
                <Link to="/"><Button style={styles.button}>Início</Button></Link>
                <Link to="/contato"><Button style={styles.button}>Contato</Button></Link>
                <Link to="/sobre"><Button style={styles.button}>Sobre</Button></Link>
                <Link to="/como-ajudar"><Button style={styles.button}>Como ajudar</Button></Link>

                <div style={{flexDirection: 'row', display: 'flex', justifyContent: 'center', flex: 1}}>

                <Autocomplete
                    id="combo-box-demo"
                    onOpen={() => {
                        history.push('');
                    }}
                    onChange={(event,value) => {
                        if( value != null){
                            const index = getIndexFromID( value.id );
                            props.markersRef.current[index].props['onClick']();
                            props.markersRef.current[index].leafletElement.openPopup();
                        }
                    }}
                    options={props.markers}
                    getOptionLabel={(option) => option.name}
                    style={{ width: 210 }}
                    renderInput={(params) => {
                        params['InputProps']['startAdornment'] = <InputAdornment position="start"><SearchIcon /></InputAdornment>;
                        params['InputProps']['className'] += ` ${classes.underline}`;
                        return(
                            <TextField
                            {...params}
                            style={{
                                backgroundColor: 'rgb(223, 223, 223)',
                                borderRadius: 0,
                                padding: 5,
                            }}
                            placeholder="Pesquise"
                            />
                        )
                    }}
                    />

                </div>
                <div style={{...styles.menu, ...styles.account}} >
                    {user ?
                    <>
                        <div 
                        onClick={() => {
                            history.push('');
                            props.openAddMarker();
                        }}
                        style={{height: 40, paddingTop: 8,paddingRight: 10, textAlign: 'right'}}>
                        <Button variant="contained" style={{
                            background: 'rgb(174, 77, 163)',
                            color: '#fff',
                            fontSize: 11,
                        }}>Cadastrar</Button>
                        </div>

                        <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} style={{color: "rgba(188, 192, 208, 0.87)"}}>
                            <AccountCircleIcon />
                        </Button>
                        
                        <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            style={{marginTop: 25}}
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={showProfile}>Visualizar Perfil</MenuItem>
                            <MenuItem onClick={logout}>Sair</MenuItem>
                        </Menu>
                        
                    </>
                    :
                    <>
                        <div 
                        onClick={() => openRegisterDialog()}
                        style={{height: 40, paddingTop: 8,paddingRight: 10, textAlign: 'right'}}>
                        <Button variant="contained" style={{
                            background: 'rgb(140, 138, 140) none repeat scroll 0% 0%',
                            color: '#fff',
                            fontSize: 11,
                        }}>Cadastrar</Button>
                        </div>

                        <AccountCircleIcon onClick={() => openAuthDialog()} />
                    </>
                    }
                    
                </div>
            </div>
        </div>
        :
        <>
        <div style={styles.header}>
            <div style={{...styles.menu, ...{
                flex: 1
            }}}>
                <Link to="/"><img alt="logo" src={logo_image} style={styles.logo}/></Link>
            </div>
            <div
            onClick={() => setModalOpen(true)}
            style={{
                cursor: 'pointer',
                dispaly: 'flex', 
                marginRight: 5,
                padding: 10, 
                justifyContent: 'center'}}>
                <MenuIcon/>
            </div>
        </div>

        <Drawer anchor={'right'} open={isModalOpen} onClose={() => setModalOpen(false)}>
            <List>
                <ListItem button component={Link} to="/">
                    <ListItemText primary={'Início'} />
                </ListItem>
                <ListItem button component={Link} to="/contato">
                    <ListItemText primary={'Contato'} />
                </ListItem>
                <ListItem button component={Link} to="/sobre">
                    <ListItemText primary={'Sobre'} />
                </ListItem>
                <ListItem button component={Link} to="/como-ajudar">
                    <ListItemText primary={'Como ajudar'} />
                </ListItem>
            </List>
        </Drawer>
        </>
        }
        </>
    );

}

const styles ={
    logo:{
        padding: 5,
        height: 40
    },
    button: {
        textTransform: 'none',
        color: 'rgb(55, 55, 55)',
        fontSize: '0.775rem',
    },
    header: {
        display: 'flex',
        backgroundColor: 'rgb(253, 247, 247)',
        dispaly: 'flex',
        height: 50,
        color: "#000",
        flexDirection: 'row',
        borderBottom: '1px solid rgba(117, 112, 112, 0.27)'
    },
    menu: {
        display: 'flex',
        height: '100%',
        alignItems: 'center'
    },
    center:{
        display: 'flex',
        flex: 1,
    },
    account:{
        marginRight: 10,
        cursor: "pointer",
    },
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
  
    return {
        loggingIn
    };
}

Header.propTypes = {
    openAddMarker: PropTypes.func,
    showProfile: PropTypes.func,
}

export default connect(mapStateToProps)(Header)