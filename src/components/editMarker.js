import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Autocomplete from '@material-ui/lab/Autocomplete';
import municipios from "address/municipios";
import estados from "address/estados";
import ClickInMarker from 'components/clickInMap';
import Modal from 'react-modal';
import CancelIcon from '@material-ui/icons/Cancel';

import { useMediaQuery } from 'react-responsive'
import { userService } from '../_services';
import different_options from 'assets/img/different_options.png';
import happy_announcement from 'assets/img/happy_announcement.png';
import { connect } from 'react-redux';
import { alertActions } from '_actions';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    width: '100%',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ['Selecione uma operação', 'Altere os dados', 'Deixe uma observação'];
}

const edit_types = [
    {icon: EditIcon, title: 'Editar'},
    {icon: DeleteIcon, title: 'Remover'},
];

function EditMarker(props) {

  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [submission_in_progress, setSubmissionProgress] = React.useState(true);
  const [editType, setEditType] = React.useState(0);
  const steps = getSteps();
  const [addressByClick, setAddressByClick] = useState(false);

  const [formMarker, setFormMarker] = useState({
    name: '',
    address: '',
    num_address: '',
    city: '',
    state: '',
    country: 'Brasil',
    website: '',
    phone: '',
    observation: '',
  });

  const [formError, setFormError] = useState({
    name: false,
    address: false,
    city: false,
    state: false,
    country: false,
    website: false,
    phone: false,
  });

  const { selectedMarker } = props;

  useEffect(() => {

    setFormMarker({
      name: secureString(selectedMarker['name']),
      address: secureString(selectedMarker['address']),
      num_address: secureString(selectedMarker['num_address']),
      city: secureString(selectedMarker['city']),
      state: secureString(selectedMarker['state']),
      country: 'Brasil',
      website: secureString(selectedMarker['website']),
      phone: secureString(selectedMarker['phone']),
    })
  },[ selectedMarker ]);

  const secureString = ( param ) => {
    return param ? param : '';
}

  const submitForm = () => {

    if( editType === 0 ){

      const params = {
        token: selectedMarker['token'],
        name: formMarker['name'],
        address: formMarker['address'],
        num_address: formMarker['num_address'],
        city: formMarker['city'],
        state: formMarker['state'],
        country: formMarker['country'],
        website: formMarker['website'],
        phone: formMarker['phone'],
        observation: formMarker['observation'],
      };

      const { dispatch } = props;
      userService.sendRequest('/api/marker/update', 'PUT', params).then((res) => {
        dispatch( alertActions.updateMarkers() )
        setSubmissionProgress(false);
      });
    }
    else{

      const params = {
        id: selectedMarker['id'],
        observation: formMarker['observation'],
      };

      userService.sendRequest('/api/marker/delete', 'DELETE', params).then((res) => {
        setSubmissionProgress(false);
      });

    }
  }

  const customHandleChange = (updated) => {
    setFormMarker({...formMarker, ...updated});
  };

  const handleChange = prop => event => {
    const value = (event.target && event.target.value) ? event.target.value : "";
    if(prop === 'num_address' && !(/^\d{0,6}$/.test(value))) return false;

    setFormError({...formError, [prop]: false})
    setFormMarker({...formMarker, [prop]: value});
  };

  const handleChangeCity = ( value ) => {
    let target_city = '';
    let target_state = "";

    if( value !== null 
      && typeof value !== "undefined" 
      && typeof value.nome !== "undefined") 
      target_city=value.nome;

    if( target_city.length > 0 ){
      const obj_city = municipios.filter((item) => item.nome === target_city );
      const city_state = estados.filter((item) => item.codigo_uf === obj_city[0].codigo_uf)
      target_state = city_state[0].uf;
    } else{
      target_state = formMarker['state'];
    }

    setFormError({...formError, 'city': false, 'state': false})
    setFormMarker({...formMarker, 'city': target_city, 'state': target_state});
  }

  const handleChangeState = (value) => {

    let target_state = '';

    if( value !== null 
      && typeof value !== "undefined" 
      && typeof value.uf !== "undefined") 
      target_state=value.uf;

    setFormError({...formError, 'state': false})
    setFormMarker({...formMarker, 'state': target_state, 'city': ''});
  };


  const checkForm = () => {
    var result = true;
    ['name', 'address', 'city', 'state', 'country'].reverse().forEach((prop) => {
      if( formMarker[ prop ].length === 0){
        setFormError({
          ...formError,
          [prop]: true,
        })
        result = false;
        return;
      }
    })
    return result;
  }

  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 600px)' })

  function getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
            <div style={{display: 'flex', flex: 1, flexDirection: 'row'}}>
                {!isTabletOrMobile &&
                <img alt="different options" style={{height: 200}} src={different_options} />
                }
              <div>
                  <p>O que deseja fazer?</p>
                  <div style={{display: 'flex', flexDirection: 'row'}}>
                      {edit_types.map((item, index) => {

                          const selected = editType === index;
                          
                          return(
                            <div style={selected ? styles.selected_block : styles.block} onClick={() => setEditType(index)}>
                                <item.icon style={selected ? styles.selected_blockIcon: styles.blockIcon} />
                                <p style={selected ? styles.selected_text : styles.text}>{item.title}</p>
                            </div>
                          )
                      })}
                  </div>
              </div>
            </div>
        )
      case 1:
        return(
          <form style={{padding: 10}} >
            <Grid container spacing={2}>
              <Grid item md={12} xs={12}>
                  <TextField 
                  InputLabelProps={{
                      shrink: true,
                  }} 
                  variant="outlined" 
                  fullWidth label="Nome" 
                  onChange={handleChange('name')}
                  value={formMarker['name']}
                  error={formError['name']}
                  required />
              </Grid>
              <Grid item md={6} xs={12}>
                  <TextField 
                  fullWidth 
                  error={formError['address']}
                  label="Rua" onChange={handleChange('address')} value={formMarker['address']} required
                  InputLabelProps={{
                      shrink: true,
                  }} 
                  variant="outlined"  />
              </Grid>
              <Grid item md={6} xs={12}>
                  <TextField 
                  error={formError['num_address']}
                  fullWidth 
                  label="Número"
                  onChange={handleChange('num_address')} value={formMarker['num_address']}
                  InputLabelProps={{
                      shrink: true,
                  }} 
                  variant="outlined"  />
              </Grid>
              <Grid item md={4} xs={6}>
                <Autocomplete
                  fullWidth
                  onChange={(evt,value) => {handleChangeCity(value)}}
                  value={formMarker['city']}
                  options={municipios}
                  getOptionLabel={(option) => typeof option === 'string' ?  option : option.nome}
                  renderInput={(params) => <TextField {...params} error={formError['city']} required label="Cidade" InputLabelProps={{ shrink: true }}  variant="outlined" />}
                />
              </Grid>
              <Grid item md={4} xs={6}>
                <Autocomplete
                  fullWidth
                  onChange={(evt,value) => {handleChangeState(value)}}
                  value={formMarker['state']}
                  options={estados}
                  getOptionLabel={(option) => typeof option === 'string' ?  option : option.uf}
                  renderInput={(params) => <TextField {...params} error={formError['state']} required label="Estados" InputLabelProps={{ shrink: true }}  variant="outlined" />}
                />
              </Grid>
              <Grid item md={4} xs={12}>
                  <TextField 
                  fullWidth 
                  disabled
                  error={formError['country']}
                  label="País" onChange={handleChange('country')} value={formMarker['country']} required
                  InputLabelProps={{
                      shrink: true,
                  }} 
                  variant="outlined"  />
              </Grid>
              <Grid item md={8} xs={12}>
                  <TextField 
                  fullWidth 
                  error={formError['website']}
                  label="Website" onChange={handleChange('website')} value={formMarker['website']}
                  InputLabelProps={{
                      shrink: true,
                  }} 
                  variant="outlined"  />
              </Grid>
              <Grid item md={4} xs={12}>
                  <TextField
                  fullWidth
                  error={formError['phone']}
                  label="Número para contato"
                  onChange={handleChange('phone')} value={formMarker['phone']}
                  InputLabelProps={{
                      shrink: true,
                  }} 
                  variant="outlined"  />
              </Grid>
          </Grid>
          </form>
        );
      case 2:
        return(
            <div style={{margin: 15}}>
                <p>Comente sobre a {editType===0? 'alteração' : 'remoção'}</p>
                <TextField
                    id="outlined-multiline-static"
                    label="Observação"
                    multiline
                    onChange={handleChange('observation')} 
                    value={formMarker['observation']}
                    fullWidth
                    rows={4}
                    defaultValue=""
                    variant="outlined"
                    />
            </div>
        );
      default:
        return 'Unknown stepIndex';
    }
  }

  const handleNext = () => {
    
    if(activeStep === 0 && editType===1){
      setActiveStep((prevActiveStep) => prevActiveStep + 2);
    }
    else if(activeStep !== 1 || checkForm() ){
      setActiveStep((prevActiveStep) => prevActiveStep + 1);

      if(activeStep === 2){
        submitForm();
      }

    }
  };

  const handleBack = () => {
    if(activeStep === 2 && editType===1){
      setActiveStep((prevActiveStep) => prevActiveStep - 2);  
    }
    else{
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
    }
  };

  const handleReset = () => {
    setSubmissionProgress(true);
    setActiveStep(0);
  };

  return (
    <div className={classes.root}>
      <span onClick={() => props.close()} style={styles.close}><CancelIcon style={{color: "#333"}}/></span>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div style={{display: 'flex', flex: 1}}>
        {activeStep === steps.length ? (
          <div>
            {submission_in_progress ?
            <>
              <p>Carregando...</p>
            </>
            :
            <>
              <img alt="edit" src={happy_announcement} style={{height: 200}} />          
              <Button onClick={handleReset}>Editar novamente</Button>
            </>
            }
          </div>
        ) : (
          <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            <div style={{display: 'flex', flex: 1, alignItems: 'center', flexDirection: 'column'}}>
                <div className={classes.instructions}>{getStepContent(activeStep)}</div>
            </div>
            <div style={{display: 'flex', marginBottom: 20}}>
              <div style={{display: 'flex'}}>
                {activeStep===1 &&
                  <Button style={{marginLeft: 10}} onClick={() => setAddressByClick(true)}>Não sabe o endereço?</Button>
                }
              </div>
              <div style={{
                  display: 'flex',
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'end',
                  marginRight: 25
              }}>
                <Button
                  disabled={activeStep === 0}
                  onClick={handleBack}
                  className={classes.backButton}
                >
                  Voltar
                </Button>
                <Button variant="contained" color="primary" onClick={handleNext}>
                  {activeStep === steps.length - 1 ? 'Finalizar' : 'Continuar'}
                </Button>
            </div>
            </div>
          </div>
        )}
      </div>

      <Modal
      isOpen={addressByClick}
      onRequestClose={() => {setAddressByClick(false)}}
      style={styles.modal}
      contentLabel="Modal"
      >
          <ClickInMarker
          handleChange={customHandleChange}
          close={() => setAddressByClick(false)}
            />
      </Modal>
    </div>
  );
}

const styles = {
    block: {
        padding: 10,
        border: '1px solid #dddbdb',
        borderRadius: 5,
        marginLeft: 5,
        width: 80,
        textAlign: 'center',
        cursor: 'pointer'
    },
    selected_block: {
        padding: 10,
        backgroundColor: '#9f559e',
        border: '1px solid #dddbdb',
        borderRadius: 5,
        marginLeft: 5,
        width: 80,
        textAlign: 'center',
        cursor: 'pointer'
    },
    blockIcon: {
        color: '#bdb8b8'
    },
    selected_blockIcon: {
        color: '#fff'
    },
    selected_text: {
        margin: 0,
        color: "#fff",
        'WebkitTouchCallout': 'none',
    'WebkitUserSelect': 'none',
     'KhtmlUserSelect': 'none',
       'MozUserSelect': 'none',
        'msUserSelect': 'none',
            'userSelect': 'none' 
    },
    text: {
        margin: 0,
        color: "#999",
        'WebkitTouchCallout': 'none',
    'WebkitUserSelect': 'none',
     'KhtmlUserSelect': 'none',
       'MozUserSelect': 'none',
        'msUserSelect': 'none',
            'userSelect': 'none' 
    },
    close:{
      position: 'absolute',
      top: 10,
      right: 10,
      cursor: 'pointer'
    }
}

export default connect()(EditMarker);