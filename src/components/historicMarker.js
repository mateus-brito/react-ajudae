import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import not_found_image from 'assets/img/not_found.svg';
import { connect } from 'react-redux';
import { alertActions } from '_actions';
import CancelIcon from '@material-ui/icons/Cancel';

import { userService } from '../_services';
import { Button } from '@material-ui/core';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
  title: {
    marginRight: 10,
    '&::before': {
      content: '"Nome: "',
      display: 'inline-block',
      fontWeight: 'bold',
      marginRight: 3
    }
  },
  address: {
    marginRight: 10,
    '&::before': {
      content: '"Endereço: "',
      display: 'inline-block',
      fontWeight: 'bold',
      marginRight: 3
    }
  },
  city: {
    marginRight: 10,
    '&::before': {
      content: '"Cidade: "',
      display: 'inline-block',
      fontWeight: 'bold',
      marginRight: 3
    }
  },
  state: {
    marginRight: 10,
    '&::before': {
      content: '"Estado: "',
      display: 'inline-block',
      fontWeight: 'bold',
      marginRight: 3
    }
  },
  country: {
    marginRight: 10,
    '&::before': {
      content: '"País: "',
      display: 'inline-block',
      fontWeight: 'bold',
      marginRight: 3
    }
  },
  website: {
    marginRight: 10,
    '&::before': {
      content: '"Site: "',
      display: 'inline-block',
      fontWeight: 'bold',
      marginRight: 3
    }
  },
  phone: {
    marginRight: 10,
    '&::before': {
      content: '"Telefone: "',
      display: 'inline-block',
      fontWeight: 'bold',
      marginRight: 3
    }
  },
  code: {
    '&::before': {
      content: '"#"',
      color: 'rgba(98, 96, 96, 0.87)',
    }
  }
});

function getMonth( month ){
  if( month <= 9) return '0'+month;
  else return month;
}

function Row(props) {
  const { row } = props;

  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();
  const date = new Date(row.updated_on);

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell align='center'>{row.author}</TableCell>
        <TableCell align="center">{`${date.getDate()}/${getMonth(1+date.getMonth())}/${date.getFullYear()}`}</TableCell>
        <TableCell align='center'>{row.observation}</TableCell>

      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <div style={{display: 'flex', flexDirection: 'row'}}>
                <Typography variant="h6" gutterBottom component="div" style={{color: 'rgba(98, 96, 96, 0.87)', display: 'flex',flex: 1}}>
                  Histórico
                </Typography>
                <span className={classes.code}>{row.id}</span>
              </div>
              <div>
                <p className={classes.title}>{row.name}</p>
                <p className={classes.address}>{row.address}{row.num_address ? ', '+row.num_address: ''}</p>
                <p><span className={classes.city}>{row.city}</span><span className={classes.state}>{row.state}</span><span className={classes.country}>{row.country}</span></p>
                <p className={classes.website}>{row.website}</p>
                <p className={classes.phone}>{row.phone}</p>
              </div>
              <Button onClick={() => props.askToRestore( row.id )} variant="contained" color="default">Restaurar</Button>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    date: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
};

function HistoricMarker(props) {

  const [isOpenConfirmDialog, setOpenConfirmDialog] = React.useState(false);
  const [markerID, setMarkerID] = React.useState(false);
  const [rows, setRows] = useState([]);

  const { target_token } = props;

  useEffect(() => {
    const loadHistoric = async() => {
  
      if( target_token ){
        userService.sendRequest(`/api/marker/historic/${target_token}`, 'GET').then((res) => {
          var result = res.map((item) => item);
          setRows(result);
        });
      }
    }

    loadHistoric();
  }, [target_token]);

  const handleCloseDialog = () => {
    setOpenConfirmDialog(false);
  }

  function restoreToId( target_id ) {
    const params = {
      id: target_id
    }
    const { dispatch } = props;
    userService.sendRequest('/api/marker/restore', 'PUT', params).then((res) => {
      dispatch( alertActions.updateMarkers() )
      setOpenConfirmDialog(false);
    });
  }

  const askToRestore = ( target_id ) => {
    setMarkerID( target_id );
    setOpenConfirmDialog( true );
  }

  return (
    <>
    {rows.length > 0 ? 
    <>
    <span onClick={() => props.close()} style={styles.close}><CancelIcon style={{color: "#333"}}/></span>
    <TableContainer>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow style={{borderBottom: '1px solid rgba(224, 224, 224, 1)'}}>
            <TableCell />
            <TableCell align='center'>Autor</TableCell>
            <TableCell align="center">Data</TableCell>
            <TableCell align='center'>Observação</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <Row askToRestore={askToRestore} key={row.name} row={row} classes={props.classes} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </>
    :
    <div>
      <h3 style={{
        color: '##443d4a',
        fontSize: 23,
        textAlign: 'center', 
        width: '100%', 
        marginBottom: 50}}>Histórico não encontrado</h3>
        <span onClick={() => props.close()} style={styles.close}><CancelIcon style={{color: "#333"}}/></span>
      <img alt="not found" src={not_found_image} style={{width: '100%', height: 300}}/>
    </div>
    }

      <Dialog
        open={isOpenConfirmDialog}
        onClose={handleCloseDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Restaurar dados"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Deseja restaurar as dados do marcador para uma versão anterior? #{markerID}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog} color="primary">
            Cancelar
          </Button>
          <Button onClick={() => restoreToId(markerID)} color="primary" autoFocus>
            Confirmar
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

const styles = {
  close:{
    position: 'absolute',
    top: 10,
    right: 10,
    cursor: 'pointer'
  }
}

HistoricMarker.propTypes = {
  target_token: PropTypes.string,
  close: PropTypes.func,
};

export default connect()(HistoricMarker);