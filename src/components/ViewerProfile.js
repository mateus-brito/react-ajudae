import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import default_profile from "assets/img/default_profile.png";
import { userService } from '_services';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import { connect } from 'react-redux';
import CancelIcon from '@material-ui/icons/Cancel';

function ViewerProfile(props)
{
    const [markers, setMarkers] = useState([]);
    const [ user, setUser ] = useState(null);

    useEffect(() => {
        const user_temp = JSON.parse(localStorage.getItem('user'));
        setUser( user_temp );
    },[props.loggingIn]);

    useEffect(() => {
        loadProfile();
    });

    const loadProfile = () => {
        userService.sendRequest(`/api/marker/get/user/${props.user_id}`, 'GET').then((res) => {
            setMarkers( res );
        });
    }

    return(
        <div>
            <span onClick={() => props.close()} style={styles.close}><CancelIcon style={{color: "#fff"}}/></span>
            <div style={{backgroundColor: '#7800FF', height: 150}}></div>
            <Grid container style={{marginTop: -85}}>
                <Grid item md={2} xs={12}>
                    <div style={{padding: 10, textAlign: 'center'}}>
                        <img alt="profile" style={{height: 150}} src={default_profile} />
                        <span style={{width: '100%', display: 'block'}}>{user ? user.username : '...'}</span>
                    </div>
                </Grid>
                <Grid item md={9} xs={12}>
                    <div style={{marginTop: 45}}>
                        <h3 style={{color: '#fff'}}>CONTRIBUIÇÕES</h3>
                        <div style={{marginTop: 20, display: 'flex', justifyContent: 'center'}}>
                            {markers.length === 0 &&
                            <p style={{padding: 10, backgroundColor: 'rgb(179, 171, 177)', color: 'rgb(121, 109, 118)'}}>
                            Nenhuma instituição cadastrada ou atualizada
                            </p>
                            }

                            <div style={{display: 'flex', flexDirection: 'column', width: '100%', marginTop: 10, cursor: 'pointer'}}>
                                {markers.map((item) => {
                                    return(
                                        <div 
                                        style={{
                                            backgroundColor: 'rgb(219, 214, 214)',
                                            marginBottom: 3,
                                            paddingRight: 5,
                                            paddingLeft: 5,
                                            display: 'flex',
                                            flexDirection: 'row',
                                            alignItems: 'center'
                                        }}>
                                            <div style={{padding: 8}}>
                                                <HomeWorkIcon />
                                            </div>
                                            <div>
                                                <p>{item.name}</p>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}

const styles = {
    image: {
        width: '100%',
    },
    close:{
        position: 'absolute',
        top: 10,
        right: 10,
        cursor: 'pointer'
    }
};

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
  
    return {
        loggingIn
    };
}

export default connect(mapStateToProps)(ViewerProfile);