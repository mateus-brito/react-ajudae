import React, { useEffect,useState } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { connect } from 'react-redux';
import HistoryIcon from '@material-ui/icons/History';
import EditIcon from '@material-ui/icons/Edit';
import ReportProblemIcon from '@material-ui/icons/ReportProblem';
import { userActions } from '_actions';
import { history } from '_helpers';

import {
    TelegramShareButton,
    TelegramIcon,
    WhatsappShareButton,
    WhatsappIcon,
    LinkedinShareButton,
    LinkedinIcon,
    TwitterShareButton,
    TwitterIcon,
    FacebookShareButton,
    FacebookIcon
} from "react-share";

function InfoMarker(props)
{
    const { token, name, description, address, num_address, city, state, country, rating, total_rating, user_id, by_username } = props.selectedMarker;

    const [ user, setUser ] = useState(null);
    
    useEffect(() => {
        const temp_user = JSON.parse(localStorage.getItem('user'));
        setUser( temp_user );
    },[props.loggingIn])

    const comma = (one, two) => {
        return (one && two) ? ', ' : '';
    }

    const openAuthDialog = () => {
        const { dispatch } = props;
        dispatch(userActions.openDialog());
    }

    const openEditModal = async() => {
        if( user != null ){
            props.openEditMarker();
        }
        else{
            const { dispatch } = props;
            await dispatch( userActions.setAfterLogin(props.openEditMarker) );
            openAuthDialog();
        }
    }

    const openProfile = () => {
        if( user_id == null ){
            //props.openProfile( user_id );
        }
        else {
            props.openProfile( user_id );
        }
    }

    const denounce = () => {
        history.push(`/contato?tipo=denuncia&target=${token}`);
    }

    const current_url = "https//:"+window.location.host+window.location.pathname+"?post="+token;

    if(name.length === 0) return (<></>);

    return(
        <div style={{width: 300, backgroundClip: '#fff', height: 'calc(100vh - 50px)', padding: 15, borderRight: '1px solid #bfbfbf'}}>
            <div style={{display: 'flex', flexDirection: 'row'}} onClick={props.close}>
                <p style={{color: '#3e3c3c',fontWeight: 'bold', margin: 0, fontSize: 19}}>{name.toUpperCase()}</p>
                <CloseIcon style={{cursor: 'pointer'}} />
            </div>
            <p onClick={openProfile} style={styles.author}>{by_username}</p>

            <div>
                <p style={{
                    color: 'rgb(162, 154, 154)',
                    fontSize: 14,                    
                    margin: 0, 
                    }}>{description}</p>
            </div>

            <div style={{textAlign: 'left', margin: '40px 0'}}>                
                <p style={styles.text}>{address}{comma(address, num_address)}{num_address}</p>
                <p style={styles.text}>{city}{comma(city, state)}{state}</p>
                <p style={styles.text}>{country}</p>
            </div>

            <div style={{display:'flex', flexDirection: 'row', width: '100%'}}>
                <div style={styles.menuButton} onClick={props.openHistoricModal}>
                    <HistoryIcon style={styles.menuButtonIcon} />
                    <p style={styles.menuButtonText}>Histórico</p>
                </div>
                <div style={styles.menuButton} onClick={openEditModal}>
                    <EditIcon style={styles.menuButtonIcon} />
                    <p style={styles.menuButtonText}>Editar</p>
                </div>
                <div style={styles.menuButton} onClick={denounce}>
                    <ReportProblemIcon style={{...styles.menuButtonIcon, ...{color: 'red'}}} />
                    <p style={styles.menuButtonText}>Denunciar</p>
                </div>
            </div>
            
            <div style={{marginTop: 20, textAlign: 'center'}}>
            <FacebookShareButton style={styles.button}
                url={current_url}
                title={name}
            >
                <FacebookIcon size={32} round />
            </FacebookShareButton>

            <TelegramShareButton style={styles.button}
                url={current_url}
                title={name}
            >
            <TelegramIcon size={32} round />
          </TelegramShareButton>

            <WhatsappShareButton style={styles.button}
                url={current_url}
                title={name}
            >
            <WhatsappIcon size={32} round />
          </WhatsappShareButton>

            <TwitterShareButton style={styles.button}
                url={current_url}
                title={name}
            >
            <TwitterIcon size={32} round />
          </TwitterShareButton>

            <LinkedinShareButton style={styles.button}
                url={current_url}
                title={name}
            >
            <LinkedinIcon size={32} round />
          </LinkedinShareButton>
          </div>
        </div>
    )
}

const styles = {
    text: {
        fontSize: 13,
        margin: '5px 0',
    },
    title:{
        color: '#2f5577',
        fontSize: 18
    },
    click: {
        color: '#911e44',
        cursor: 'pointer',
    },
    menuButton: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flex: 1,
        cursor: 'pointer'
    },
    menuButtonIcon: {
        fontSize: 20,
        color: '#b5b5b5'
    },
    menuButtonText: {
        fontSize: 15,
        color: '#888'
    },
    author: {
        color: '#1094c8',
        cursor: 'pointer',
        textTransform: 'capitlize'
    },
    button: {
        padding: 3
    }
}

InfoMarker.propTypes = {
    selectedMarker: PropTypes.object,
    close: PropTypes.func,
    openEditMarker: PropTypes.func,
    openHistoricModal: PropTypes.func,
    showCommentModal: PropTypes.func
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
  
    return {
        loggingIn
    };
}

export default connect(mapStateToProps)(InfoMarker);