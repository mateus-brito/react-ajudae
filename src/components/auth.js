import React from 'react';
import TextField from '@material-ui/core/TextField';
import CancelIcon from '@material-ui/icons/Cancel';
import logo_image from "../assets/img/logo.png";
import { Button } from '@material-ui/core';
import login_cover from '../assets/img/login_cover.jpg';
import { connect } from 'react-redux';
import { userActions } from '_actions';
import Hidden from '@material-ui/core/Hidden';

function Auth(props){

    const [modalIsRegister, setIsRegister] = React.useState(false);
    const [userConf, setUserConf] = React.useState({
        password: '',
        email: '',
        username: '',
    });

    const handleChange = prop => event => {
        setUserConf({...userConf, [prop]: event.target.value});
    };

    const submitLogin = (e) => {
        e.preventDefault();
  
        const { dispatch } = props;
        if (!modalIsRegister) {
            dispatch(userActions.login(userConf.email, userConf.password));
        }
    }

    const closeDialog = () => {
        const { dispatch } = props;
        dispatch(userActions.closeDialog());
    }

    const submitRegister = (e) => {
        e.preventDefault();
  
        const { dispatch } = props;
        if (modalIsRegister) {
            dispatch(userActions.register(userConf.email, userConf.username,userConf.password));
        }
    }

    return(
        <div style={styles.modalContainer}>
            <div style={styles.authForm}>
                <img alt="logo" src={logo_image} style={styles.logo}/>
                <span style={{paddingLeft: 20, paddingRight: 20, textAlign: 'center'}}>Faça login na sua conta para participar e contribuir no site.</span>
                {modalIsRegister ?
                <form style={styles.formLogin} onSubmit={submitRegister}>
                    <TextField onChange={handleChange('email')} id="outlined-basic" style={styles.input} InputProps={{style: styles.inputContainer}} label="Email" variant="outlined" />
                    <TextField onChange={handleChange('username')} id="outlined-basic" style={styles.input} InputProps={{style: styles.inputContainer}} label="Username" variant="outlined" />
                    <TextField type="password" onChange={handleChange('password')} id="outlined-basic" style={styles.input} InputProps={{style: styles.inputContainer}} label="Password" variant="outlined" />
                    <span>{props.errorRegister}</span>
                    <Button type="submit" variant="contained" color="primary" >Registrar</Button>
                    <span>Já possuí uma conta? <span onClick={() => setIsRegister(false)}>Login</span></span>
                </form>
                :
                <form style={styles.formLogin} onSubmit={submitLogin}>
                    <TextField onChange={handleChange('email')} id="outlined-basic" style={styles.input} InputProps={{style: styles.inputContainer}} label="Email" variant="outlined" />
                    <TextField type="password" onChange={handleChange('password')} id="outlined-basic" style={styles.input} InputProps={{style: styles.inputContainer}} label="Password" variant="outlined" />
                    <span>{props.errorLogin}</span>
                    <Button type="submit" variant="contained" color="primary" >Login</Button>
                    <span>Ainda não tem uma conta? <span onClick={() => setIsRegister(true)}>Cadastre-se</span></span>
                </form>
                }
                <Hidden mdUp>
                    <span onClick={() => closeDialog()} style={styles.close}><CancelIcon style={{color: "#000"}}/></span>
                </Hidden>
            </div>
            <Hidden smDown>
                <div style={styles.authInfo}>
                    <span onClick={() => closeDialog()} style={styles.close}><CancelIcon style={{color: "#fff"}}/></span>
                </div>
            </Hidden>
        </div>
    )
}

const styles = {
    authInfo: {
        dispaly: 'flex',
        width: '100%',
        height: '100%',
        background: `linear-gradient(0deg,rgba(37,40,57,0.9),rgba(37,40,57,0.9)),url(${login_cover})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat'
    },
    modalContainer:{
        display: 'flex',
        width: '100%',
        height: '100%',
        flexDirection: 'row'
    },
    modal : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    },
    authForm: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
      },
    logo: {
        height: 60,
        marginBottom: 10
    },
    formLogin:{
        paddingTop: 10,
        display: 'flex',
        flexDirection: 'column',
        width: '65%'
    },
    inputContainer:{
        borderRadius: 50
    },
    input:{
        marginBottom: 10
    },
    close:{
        position: 'absolute',
        top: 10,
        right: 10,
        cursor: 'pointer'
    }
};

function mapStateToProps(state) {
    const { loggingIn, errorRegister, errorLogin } = state.authentication;

    return {
        loggingIn,
        errorRegister,
        errorLogin
    };
}
  
const connectedAuth = connect(mapStateToProps)(Auth);
  
export default (connectedAuth);