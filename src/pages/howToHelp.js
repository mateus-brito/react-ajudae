import React from 'react';
import friends_online from 'assets/img/friends_online.svg';
import savings from 'assets/img/savings.svg';
import code_review from 'assets/img/code_review.svg';
import Grid from '@material-ui/core/Grid';

function HowToHelp(props){

    return(
    <>
        <Grid container>
            <Grid item xs={12}>
                <div style={{
                    backgroundColor: '#eae6e6',
                    display: 'flex',
                    padding: 10,
                    justifyContent: 'center',
                    flexDirection: 'column',
                    color: 'rgb(124, 116, 125)',
                    fontSize: '13px',
                    marginBottom: '53px'
                }}>
                    <h1 style={{textAlign: 'center', marginBottom: 0}}>Que ajudar, mas não sabe como?</h1>
                    <p style={{textAlign: 'center', paddingBottom: 20}}>Elaboramos uma pequena lista de coisas que você pode fazer, analise-as com carinho e ajude com o que você pode!</p>
                </div>
            </Grid>
            <Grid item md={3} sm={12} style={{ padding: 20 }}>
                <img alt="friends" src={friends_online} style={{width: '100%'}} />
            </Grid>
            <Grid item md={9} sm={12} style={styles.card}>
                <h2>Doe informações</h2>
                <p style={styles.text}>Conhece organizações que ainda não estão cadastradas?</p>
                <p style={styles.text}>No ajudaê qualquer conta pode cadastrar e atualizar os dados de um local.</p>
            </Grid>
        </Grid>
        <Grid container direction="row-reverse">
            <Grid item md={3} sm={12} style={{backgroundColor: '#9dabf9', padding: 20}}>
                <img alt="saving" src={savings} style={{width: '100%'}} />
            </Grid>
            <Grid alignItems="flex-end" item md={9} sm={12} style={{...styles.card, ...{backgroundColor: '#9dabf9'}}}>
                <h2>Ajude as instituição</h2>
                <p style={styles.text}>Pode fazer uma doação?</p>
                <p style={styles.text}>Qualquer valor está ótimo, isso é uma grande ajuda!</p>
            </Grid>
        </Grid>
        <Grid container direction="row">
            <Grid item md={3} sm={12} style={{ padding: 20 }}>
                <img alt="code review" src={code_review} style={{width: '100%'}} />
            </Grid>
            <Grid item md={9} sm={12} style={styles.card}>
                <h2>Melhore nosso serviço</h2>
                <p style={styles.text}>Sabe programar? </p>
                <p style={styles.text}>No ajudaê qualquer conta pode cadastrar e atualizar os dados de um local.</p>
            </Grid>
        </Grid>
    </>
    )
}

const styles = {
    text:{
        margin: 0,
    },
    card: {
        padding: 20,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
    }
};

export default HowToHelp;