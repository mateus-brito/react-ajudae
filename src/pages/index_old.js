import React, {useEffect, useState, useRef} from 'react';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import HistoricMenu from "../components/historicMenu";
import Header from "../components/header";
import { userService } from '../_services';
import L from 'leaflet'
import Modal from 'react-modal';

import TextField from '@material-ui/core/TextField';
import CancelIcon from '@material-ui/icons/Cancel';
import { userActions } from '_actions';
import { connect } from 'react-redux';
import heart_image from '../assets/gif/heart.gif';
import login_cover from '../assets/img/login_cover.jpg';
import logo_image from "../assets/img/logo.png";
import { Button } from '@material-ui/core';
import RegisterMarker from 'components/registerMarker';
import RoomIcon from '@material-ui/icons/Room';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import HistoricMarker from 'components/historicMarker';
import EditMarker from 'components/editMarker';
import Hidden from '@material-ui/core/Hidden';
import { Shake } from 'reshake'
import InfoMarker from "components/infoMarker"

export const pointerIcon = new L.Icon({
    iconUrl: heart_image,
    iconRetinaUrl: '',
    iconSize:     [61.75, 56.5], // size of the icon
    shadowSize:   [0, 0], // size of the shadow
    iconAnchor:   [30.875, 28.25], // point of the icon which will correspond to marker's location
    shadowAnchor: [0, 0],  // the same for the shadow
    popupAnchor:  [0, -26] // point from which the popup should open relative to the iconAnchor
})

const default_select = {
  name: '',
  description: '',
  address: '',
  num_address: '',
  city: '',
  state: '',
  country: '',
}

function Index(props) {

    const markersRef = useRef([]);
    const mapRef = useRef();

    const [ user, setUser ] = useState({});
    const [isOpen, changeVisiblity] = useState(false);
    const [historicVisibility, setHistoricVisibility] = useState(false);
    const [editMarkerVisibility, setEditMarkerVisibility] = useState(false);
    
    const [ markers, setMarkers ] = useState([]);
    const [ currentItem, setCurrentItem] = useState(default_select);
    
    const [ addMarkerIsOpen,setAddMarkerIsOpen] = useState(false);
    const [modalIsOpen,setIsOpen] = React.useState(false);
    const [modalIsRegister,setIsRegister] = React.useState(false);

    const [position,setPosition] = React.useState([-16.6799, -49.255]);
    const [userConf, setUserConf] = useState({
      password: '',
      email: '',
      username: '',
    });

    useEffect(() => {
      if(mapRef.current){
        mapRef.current.leafletElement.invalidateSize();
      }
    }, [isOpen, currentItem.name])
  
    useEffect(() => {
        userService.sendRequest('/api/marker/get', 'GET').then((res) => {
          setMarkers( res )
        });

        if (isWidthUp('md', props.width)) {
          changeVisiblity(true);
        }
    }, [])

    useEffect(() => {
      markersRef.current = markersRef.current.slice(0, markers.length);
    }, [markers.length])

    useEffect(() => {
      const user_temp = JSON.parse(localStorage.getItem('user'));
      setUser( user_temp )
    },[props.loggingIn]);

    const handleChange = prop => event => {
      setUserConf({...userConf, [prop]: event.target.value});
    };

    const submitLogin = (e) => {
      e.preventDefault();

      const { dispatch } = props;
      if (!modalIsRegister) {
          dispatch(userActions.login(userConf.email, userConf.password));
      }
    }

    const submitRegister = (e) => {
      e.preventDefault();

      const { dispatch } = props;
      if (modalIsRegister) {
          dispatch(userActions.register(userConf.email, userConf.username,userConf.password));
      }
  }

  const openRegisterMarker = () => {
    setCurrentItem(default_select);
    setAddMarkerIsOpen(true);
  }

  return (
    <div style={styles.container}>
      <Header
      markers={markers}
      markersRef={markersRef}
      setAuthModal={setIsOpen}
      openAddMarker={ openRegisterMarker } />
      <div style={styles.main}>
          {isOpen &&
            <HistoricMenu
            markers={markers}
            markersRef={markersRef}
            changeVisiblity={changeVisiblity}
            setPosition={setPosition} />
          }
          <InfoMarker
          close={() => setCurrentItem(default_select)}
          selectedMarker={currentItem}
          setAuthModal={setIsOpen}
          openEditMarker={ () => setEditMarkerVisibility(true) }
          openHistoricModal={ () => setHistoricVisibility(true)}
          />
          <div style={{flexDirection: 'column', display: 'flex', flex: 1}}>
            
            <Map ref={mapRef} center={position} zoom={6} style={styles.map} >
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                />
                {markers.map((item, idx) => 
                  <Marker key={idx} ref={r => markersRef.current[idx]=r } key={`marker-${idx}`} position={[item.lat, item.lng]} icon={pointerIcon}
                  doubleClickZoom={'center'}
                  onClick={() => {
                    setCurrentItem({
                      id: item['id'],
                      name: item['name'],
                      token: item['token'],
                      description: item['description'],
                      address: item['address'],
                      num_address: item['num_address'],
                      city: item['city'],
                      state: item['state'],
                      country: item['country'],
                    })
                    setPosition([ item['lat'], item['lng'] ]);
                  }}>
                    <Popup>
                      <span>{item['name']}</span>
                    </Popup>
                    
                  </Marker>
                )}

        {!isOpen &&
          <div 
          onClick={() => changeVisiblity(true)}
          style={{
              position: 'absolute',
              top: 20,
              zIndex: 401,
              left: 60,
              padding: '10px 10px 6px 10px',
              backgroundColor: 'rgb(176, 172, 172)',
              borderRadius: 50,
              cursor: 'pointer',
              border: '1px solid rgb(115, 110, 110)'
          }}>
              <Shake 
              h={5}
              v={5}
              r={3}
              dur={470}
              int={10}
              max={35}
              fixed={true}
              fixedStop={true}
              freez={false}>
                  <RoomIcon style={{color: '#fff'}}/>
              </Shake>
          </div>
        }
            </Map>
          </div>
      </div>

      <Modal
          isOpen={(modalIsOpen && user==null)}
          onRequestClose={() => {setIsOpen(false)}}
          style={styles.modal}
          contentLabel="Example Modal"
        >
          <div style={styles.modalContainer}>
            <div style={styles.authForm}>
              <img alt="logo" src={logo_image} style={styles.logo}/>
              <span style={{paddingLeft: 20, paddingRight: 20, textAlign: 'center'}}>Faça login na sua conta para participar e contribuir no site.</span>
              {modalIsRegister ?
              <form style={styles.formLogin} onSubmit={submitRegister}>
                <TextField onChange={handleChange('email')} id="outlined-basic" style={styles.input} InputProps={{style: styles.inputContainer}} label="Email" variant="outlined" />
                <TextField onChange={handleChange('username')} id="outlined-basic" style={styles.input} InputProps={{style: styles.inputContainer}} label="Username" variant="outlined" />
                <TextField type="password" onChange={handleChange('password')} id="outlined-basic" style={styles.input} InputProps={{style: styles.inputContainer}} label="Password" variant="outlined" />
                <span>{props.errorRegister}</span>
                <Button type="submit" variant="contained" color="primary" >Registrar</Button>
                <span>Já possuí uma conta? <span onClick={() => setIsRegister(false)}>Login</span></span>
              </form>
              :
              <form style={styles.formLogin} onSubmit={submitLogin}>
                <TextField onChange={handleChange('email')} id="outlined-basic" style={styles.input} InputProps={{style: styles.inputContainer}} label="Email" variant="outlined" />
                <TextField type="password" onChange={handleChange('password')} id="outlined-basic" style={styles.input} InputProps={{style: styles.inputContainer}} label="Password" variant="outlined" />
                <span>{props.errorLogin}</span>
                <Button type="submit" variant="contained" color="primary" >Login</Button>
                <span>Ainda não tem uma conta? <span onClick={() => setIsRegister(true)}>Cadastre-se</span></span>
              </form>
              }
            </div>
            <Hidden smDown>
            <div style={styles.authInfo}>
              <span onClick={() => setIsOpen(false)} style={styles.close}><CancelIcon style={{color: "#fff"}}/></span>
            </div>
            </Hidden>
          </div>
      </Modal>
      
      <Modal
      isOpen={addMarkerIsOpen}
      onRequestClose={() => {setAddMarkerIsOpen(false)}}
      style={styles.modal}
      contentLabel="Example Modal"
      >
        <RegisterMarker close={() => setAddMarkerIsOpen(false)} selectedMarker={currentItem} />
      </Modal>
      
      <Modal
      isOpen={editMarkerVisibility}
      onRequestClose={() => {setEditMarkerVisibility(false)}}
      style={styles.modal}
      contentLabel="Example Modal"
      >
        <EditMarker close={() => setEditMarkerVisibility(false)} selectedMarker={currentItem} />
      </Modal>
      
      <Modal
      isOpen={historicVisibility}
      onRequestClose={() => {setHistoricVisibility(false)}}
      style={styles.modal}
      contentLabel="Example Modal"
      >
        <HistoricMarker
        close={() => {setHistoricVisibility(false)}}
        target_token={currentItem.token} />
      </Modal>
      
    </div>
  );
}

const styles = {
  container:{
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh'
  },
  main:{
    display: 'flex',
    flex: 1,
    flexDirection: 'row'
  },
  map: {
    dispaly: 'flex',
    flex: 1,
    minHeight: 400
  },
  modal : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  },
  authInfo:{
    dispaly: 'flex',
    width: '100%',
    height: '100%',
    background: `linear-gradient(0deg,rgba(37,40,57,0.9),rgba(37,40,57,0.9)),url(${login_cover})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat'
  },
  authForm: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalContainer:{
    display: 'flex',
    width: '100%',
    height: '100%',
    flexDirection: 'row'
  },
  logo: {
    height: 60,
    marginBottom: 10
  },
  formLogin:{
    paddingTop: 10,
    display: 'flex',
    flexDirection: 'column',
    width: '65%'
  },
  inputContainer:{
    borderRadius: 50
  },
  input:{
    marginBottom: 10
  },
  close:{
    position: 'absolute',
    top: 10,
    right: 10,
    cursor: 'pointer'
  }
}

function mapStateToProps(state) {
  const { loggingIn, errorRegister, errorLogin } = state.authentication;

  return {
      loggingIn,
      errorRegister,
      errorLogin
  };
}

const connectedIndex = connect(mapStateToProps)(Index);

export default withWidth()(connectedIndex);
