import React from 'react';
import baesse_profile from 'assets/img/baesse_profile.jpg';
import mateus_profile from 'assets/img/mateus_profile.jpeg';
import cover_about from 'assets/img/cover_about.jpg';
import web_development from 'assets/img/web_development.svg';

import Grid from '@material-ui/core/Grid';

function About(props){
    return (
        <>
            <div style={{
                textAlign: 'left', 
                paddingLeft: 15,
                height: 138,
                background: `linear-gradient(90deg, rgba(120, 72, 179, 0.9), rgba(187, 166, 234, 0.61)), url(${cover_about})`,
                display: 'flex',
                flexDirection: 'row',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                }}>
                <div style={{flex: 1}}>
                    <h1 style={{color: '#fbecf5'}}>Sobre nós</h1>
                    <p style={{margin: '0 0 30px 0', color: '#fde8d8'}}>Descubra um pouco sobre a origem do ajudaê e seus criadores.</p>
                </div>
            </div>
        <Grid container style={{minHeight: 'calc(100vh - 230px)'}}>
            <Grid item md={6} style={{display: 'flex'}}>
                <div style={{padding: 20, display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'flex-start'}}>
                    <h2 style={{color: '#5751B1', fontSize: 33}}>Lá vem história...</h2>
                    <p style={styles.text}>O Ajudaê inicou com a ideia do Baesse de uma maneira para que qualquer um pudesse achar facilmente locais próximos a si para ajudar. Como professor, Baesse iniciou o projeto em 2017 no IFRN de Ceará-Mirim, porém terminou ainda em uma fase muito inicial.</p>
                    <p style={styles.text}>Com a pandemia Covid-19, Baesse viu a importância de começar o projeto novamente e contou com a ajuda do amigo e desenvolvedor Mateus.</p>

                    <p style={styles.text}>Qualquer um pode ajudar com o crescimento do Ajudaê!</p>
                    <p style={styles.text}>Precisamos da sua ajuda!</p>

                </div>
            </Grid>
            <Grid item md={6} style={{display: 'flex', justifyContent: 'center'}}>
                <img alt="web development" src={web_development} style={{width: '60%'}} />
            </Grid>
            <Grid item md={6}>
                <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'start',}}>
                    <div style={styles.card}>
                        <img alt="baesse profile" src={baesse_profile} style={styles.roundImage} />
                        <p style={styles.textCard}>Baesse</p>
                    </div>
                    <div style={styles.card}>
                        <img alt="mateus profile" src={mateus_profile} style={styles.roundImage} />
                        <p style={styles.textCard}>Mateus</p>
                    </div>
                </div>
            </Grid>
        </Grid>
        </>
    );
}

const styles = {
    text: {
        color: 'rgb(49, 49, 49)',
        fontFamily: "Roboto",
        fontSize: 13,
        margin: '0 0 10px 0',
    },
    roundImage: {
        borderRadius: '50%',
        border: '2px solid #fff',
        width: 80,
    },
    textCard: {
        fontWeight: 'normal',
        color: '#222',
        fontSize: 14,
        margin: 0
    },
    card:{
        display: 'flex',
        flexDirection: 'column',
        alignSelf: 'start',
        padding: 10,
        textAlign: 'center'
    }
}

export default About;