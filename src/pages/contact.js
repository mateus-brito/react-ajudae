import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { userService } from '_services';
import Modal from 'react-modal';
import confirmation from 'assets/img/confirmation.png';
import { history } from '_helpers';

import envelope_image from 'assets/img/envelope.svg';

function Contact(props){

    const [successMessage, setSuccessMessage] = useState(false);

    const [type_contact, setTypeContact] = useState(0);
    const [token, setToken] = useState('');

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');

    useEffect(() => {
        const search = props.location.search;
        const params = new URLSearchParams(search);
        const tipo = params.get('tipo');
        const target = params.get('target');

        if( target!=null){
            setToken(target);
            setTypeContact(`${tipo}`.toLowerCase()=='denuncia');
        }
    },[])

    const submitMessage = (e) => {
        e.preventDefault();
        userService.sendRequest('/contact', 'POST', {
            'token': token,
            'name': name,
            'email': email,
            'message': message,
        }).then((res) => {
            infoSuccess();
        });
    }

    const infoSuccess = () => {
        setToken('');
        setName('');
        setEmail('');
        setMessage('');
        history.push('/contato');
        setSuccessMessage(true);
    }

    return (
        <div>
            <Grid container spacing={0} style={{minHeight: 'calc(100vh - 92px)'}}>
                <Grid item md={5} style={{display: 'flex', justifyContent: 'center'}}>
                    <img alt="envelope" src={envelope_image} style={{width: '60%'}} />
                </Grid>
                <Grid item md={7} xs={12} direction="column" style={{display: 'flex', justifyContent: 'center'}}>
                    <div style={{padding: '10px 30px'}}>
                        {type_contact==0 ? 
                        <>
                            <h2 style={{color: '#484646'}}>Entrar em contato</h2>
                            <p style={{color: '#8c8b8b'}}>Tire suas dúvidas, conte-nos suas opniões, críticas, sugestões e nos ajude a melhorar.</p>
                        </>
                        :
                        <>
                            <h2 style={{color: '#484646'}}>Faça uma denúncia</h2>
                            <p style={{color: '#8c8b8b'}}>Conte-nos o motivo da denuncia.</p>
                        </>}
                        <form autoComplete="off" onSubmit={submitMessage}>
                            <TextField value={name} required onChange={(e) => setName(e.target.value)} style={styles.textfield} fullWidth id="standard-basic" label="Seu nome" variant="filled" />
                            <TextField value={email} required onChange={(e) => setEmail(e.target.value)} style={styles.textfield} fullWidth id="standard-basic" label="Seu e-mail" variant="filled" />
                            <TextField
                            value={message}
                            required
                            fullWidth
                            onChange={(e) => setMessage(e.target.value)}
                            style={styles.textfield}
                            multiline={true}
                            id="text-message"
                            label="Escreva a sua mensagem"
                            variant="filled"
                            rows={5} />
                            <Button type="submit" variant="contained" color={type_contact == 0 ? "primary" : "secondary"}>Enviar mensagem</Button>
                        </form>
                    </div>
                </Grid>
            </Grid>

            <Modal
            isOpen={successMessage}
            onRequestClose={() => {setSuccessMessage(false)}}
            contentLabel="Auth"
            >
            <div style={{textAlign: 'center'}}>
              <img style={{width: 'auto', height: 250}} src={confirmation} alt="confirmation" />
              <h2 style={{textAlign: 'center'}}>Mensagem enviada com sucesso.</h2>
              <p style={{textAlign: 'center'}}>Nossa equipe em breve irá analisar sua mensagem, agradecemos o seu contato.</p>
            </div>
        </Modal>
        </div>
    )
}

const styles = {
    textfield: {
        marginBottom: 10
    }
}

export default Contact;