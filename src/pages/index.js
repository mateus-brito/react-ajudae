import React, {useEffect, useState, useRef} from 'react';
import HistoricMenu from "../components/historicMenu";
import InfoMarker from "components/infoMarker"
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import L from 'leaflet';
import Modal from 'react-modal';
import RegisterMarker from 'components/registerMarker';
import PropTypes from 'prop-types';

import { useMediaQuery } from 'react-responsive'
import heart_image from 'assets/gif/heart.gif';
import HistoricMarker from 'components/historicMarker';
import EditMarker from 'components/editMarker';
import CommentRate from 'components/commentRate';

const pointerIcon = new L.Icon({
    iconUrl: heart_image,
    iconRetinaUrl: '',
    iconSize:     [61.75, 56.5], // size of the icon
    shadowSize:   [0, 0], // size of the shadow
    iconAnchor:   [30.875, 28.25], // point of the icon which will correspond to marker's location
    shadowAnchor: [0, 0],  // the same for the shadow
    popupAnchor:  [0, -26] // point from which the popup should open relative to the iconAnchor
})

const default_select = {
    id: -1,
    name: '',
    token: '',
    description: '',
    address: '',
    num_address: '',
    by_username: '',
    city: '',
    state: '',
    user_id: 0,
    rating: 0,
    total_rating: 0,
    country: '',
};

function Index(props){

    const mapRef = useRef();
    const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' })

    const [isCommentModal, setCommentModal] = useState(false);
    const [isHistoricOpen, setHistoricVisiblity] = useState(true);
    const [historicVisibility, setHistoricVisibility] = useState(false);
    const [editMarkerVisibility, setEditMarkerVisibility] = useState(false);

    const [,setIsOpen] = React.useState(false);
    const [currentItem, setCurrentItem] = useState(default_select);

    const [centerPosition,setCenterPosition] = React.useState([-16.6799, -49.255]);

    const {markers, markersRef} = props;

    useEffect(() => {
        const updatedItem = markers.filter((item) => item['token'] === currentItem['token']);
        if( updatedItem.length === 1)
        {
            defineCurrentMarker( updatedItem[0] );
        }
        else{
            setCurrentItem( default_select );
        }
    }, [markers]);

    const defineCurrentMarker = ( item ) => {
        setCurrentItem({
            id: item['id'],
            rating: item['rating'],
            total_rating: item['total_rating'],
            by_username: item['by_username'],
            user_id: item['user_id'],
            name: item['name'],
            token: item['token'],
            description: item['description'],
            address: item['address'],
            num_address: item['num_address'],
            city: item['city'],
            state: item['state'],
            country: item['country'],
        })
        setCenterPosition([ item['lat'], item['lng'] ]);
    }
    
    return(
        <div style={styles.main}>
            {isHistoricOpen &&
                <HistoricMenu
                markers={markers}
                markersRef={markersRef}
                changeVisiblity={setHistoricVisiblity}
                setPosition={setCenterPosition} />
            }

            <InfoMarker
            showCommentModal={() => setCommentModal(true)}
            close={() => setCurrentItem(default_select)}
            selectedMarker={currentItem}
            setAuthModal={setIsOpen}
            openProfile={props.showProfile}
            openEditMarker={ () => setEditMarkerVisibility(true) }
            openHistoricModal={ () => setHistoricVisibility(true)}
            />

            <div style={{flexDirection: 'column', display: 'flex', flex: 1}}>
                <Map ref={mapRef} center={centerPosition} zoom={6} style={styles.map} >
                <TileLayer 
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"/>
                    {markers.map((item, idx) => 
                    <Marker ref={r => markersRef.current[idx]=r } key={`marker-${idx}`} position={[item.lat, item.lng]} icon={pointerIcon}
                    doubleClickZoom={'center'}
                    onClick={() => {
                        defineCurrentMarker(item);
                    }}>
                        <Popup>
                        <span>{item['name']}</span>
                        </Popup>
                        
                    </Marker>
                    )}

                    {!isHistoricOpen &&
                    <div 
                    onClick={() => setHistoricVisiblity(true)}
                    style={{
                        position: 'absolute',
                        top: 20,
                        zIndex: 401,
                        left: 60,
                        padding: '0 8px',
                        backgroundColor: 'rgb(89, 102, 98)',
                        cursor: 'pointer',
                        border: '1px solid rgb(255, 244, 244)',
                        color: 'white',
                        lineHeight: '12px',
                        fontWeight: 'bold',
                    }}>
                        <p>Lista de instituições</p>
                    </div>
                    }
                </Map>
            </div>
            
            <Modal
            isOpen={false}
            onRequestClose={() => {setCommentModal(false)}}
            style={isTabletOrMobile ? styles.mobileModal : styles.modal}
            contentLabel="Profile"
            >
                <CommentRate token={currentItem.token} />
            </Modal>
            
            <Modal
            isOpen={props.isOpenRegister}
            onRequestClose={() => {props.setOpenRegister(false)}}
            style={isTabletOrMobile ? styles.mobileModal : styles.modal}
            contentLabel="New Marker"
            >
                <RegisterMarker close={() => props.setOpenRegister(false)} selectedMarker={currentItem} />
            </Modal>

            <Modal
            isOpen={editMarkerVisibility}
            onRequestClose={() => {setEditMarkerVisibility(false)}}
            style={styles.modal}
            contentLabel="Example Modal"
            >
                <EditMarker close={() => setEditMarkerVisibility(false)} selectedMarker={currentItem} />
            </Modal>
            
            <Modal
            isOpen={historicVisibility}
            onRequestClose={() => {setHistoricVisibility(false)}}
            style={styles.modal}
            contentLabel="Example Modal"
            >
                <HistoricMarker
                close={() => {setHistoricVisibility(false)}}
                target_token={currentItem.token} />
            </Modal>

        </div>
    )
}

const styles = {
    main:{
      display: 'flex',
      flex: 1,
      flexDirection: 'row'
    },
    map: {
      flex: 1,
      minHeight: 'calc(100vh - 51px)'
    }
};

Index.propTypes = {
    setOpenRegister: PropTypes.func,
    showProfile: PropTypes.func,
    isOpenRegister: PropTypes.bool,
    target_profile: PropTypes.number
}

export default Index;