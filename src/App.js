import React from 'react';
import './App.css';
import { routesList } from "routes/index.js";
import { Router, Route, Switch } from "react-router-dom";
import { history } from './_helpers';

function App() {

  return (
    <Router history={history}>
        <Switch>
          {routesList.map((prop, key) => {
            return <Route path={prop.path} component={prop.component} key={key} />;
          })}
        </Switch>
    </Router>
  );
}

export default App;
